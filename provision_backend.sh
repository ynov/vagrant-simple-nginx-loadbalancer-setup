VM_NUMBER=$1

sudo apt-get install -y nginx

sudo mv /usr/share/nginx/html/index.html /usr/share/nginx/html/index-bak.html
sudo ln -sv /vopt/index.html /usr/share/nginx/html/index.html

sudo /etc/init.d/puppet stop
sudo /etc/init.d/chef-client stop
sudo update-rc.d puppet disable
sudo update-rc.d chef-client disable

echo -n "" > .bash_history
history -c
