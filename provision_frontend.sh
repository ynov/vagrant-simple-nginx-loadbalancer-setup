sudo apt-get install -y nginx

sudo cp -v /vagrant/loadbalancer_nginx_default \
    /etc/nginx/sites-enabled/default
sudo /etc/init.d/nginx restart

sudo /etc/init.d/puppet stop
sudo /etc/init.d/chef-client stop
sudo update-rc.d puppet disable
sudo update-rc.d chef-client disable

echo -n "" > .bash_history
history -c
